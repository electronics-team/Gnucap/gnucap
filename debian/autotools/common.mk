# -*- Automake -*-
#
# $Id: Makefile.am,v 26.70 2008/02/05 02:03:56 al Exp $
#
# Process this file with autoconf to produce a configure script.
#
#
#                            COPYRIGHT
#
# This file is part of "Gnucap", the Gnu Circuit Analysis Package
#
#  Copyright (C) 2019 Felix Salfelder
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

EXTRA_DIST = ${RAW_OTHER}
